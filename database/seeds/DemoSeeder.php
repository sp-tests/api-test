<?php

use Illuminate\Database\Seeder;

/**
 * Class DemoSeeder
 */
class DemoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = factory(\App\Models\User::class, 5)->create();

        foreach ($users as $user) {
            factory(\App\Models\Order::class, rand(1, 10))->create(['user_id' => $user->id]);
        }
    }
}
