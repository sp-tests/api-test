<details><summary>Задача</summary>
Нужно написать API для работы с сущностью заказов

Заказ состоит из как минимум следующих элементов:
id, user_id, status, updated_at, created_at

Необходимые методы:
Добавление нового заказа
Получение списка заказов для конкретного пользователя со smooth пагинацией (то есть записи не должны сдвигаться при пагинации если были добавлены/отредактированы записи с предыдущей страницы)
Перевод заказа в статус processed.

Есть как минимум 2 статуса: new и processed. При изменении статуса на processed нам нужно вызвать внешнее апи (можно использовать https://httpbin.org/). Считаем что внешнее API идемпотентно если мы передаем id и время когда запись была переведена в новый статус (updated_at). 
</details>

## Requirements
 * php 7.4

## Installation
 * ```composer install```
 * ```php artisan key:generate```
 * ```cp .env.example .env```
 * Configure db connection ```nano .env```
 * ```php artisan migrate --seed```

## API
 * Список заказов пользователя - ```GET /api/v1/users/{userId}/orders?page=1```
 * Создание заказа для пользователя - ```POST /api/v1/users/{userId}/orders```
 * Обработка заказа - ```POST /api/v1/orders/{orderId}/process```. Доступно только для заказов в статусе ```new```.

Коллекция для Postman: https://www.getpostman.com/collections/ad970ae617de92b37751

### Процесс обработки заказа:
 * После вызова api у заказа ставится статус ```processing```
 * Пытаемся вызвать Job ```ProcessOrder```. Сделал так, потому что клиент не должен ждать пока отработает вызов во внешний API.
   * Если был Exception(напр-р отвалился драйвер очередей), то заказу возвращаем статус ```new```
 * Задача по обработке дергает ```\App\Services\OrdersService::processOrder```, который в свою очередь вызывает запрос в API - ```\App\Services\RemoteApiService::orderApiAction```
   * Если вызов в API завершился неудачей, то возвращаем статус ```new```. Ошибку пишем в лог.
   * Если вызов был успешным, то кидаем заказ в статус ```processed```
