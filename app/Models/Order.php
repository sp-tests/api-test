<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Order
 * @package App\Models
 *
 * @property-read int $id
 * @property int $user_id
 * @property string $status
 * @property DateTimeInterface $created_at
 * @property DateTimeInterface $updated_at
 *
 * @property-read User $user
 */
class Order extends Model
{
    public const STATUS_NEW        = 'new';
    public const STATUS_PROCESSED  = 'processed';
    public const STATUS_PROCESSING = 'processing';

    protected $guarded = ['id'];
    protected $casts   = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
