<?php

namespace App\Http\Requests;

use App\Models\Order;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ProcessOrderRequest
 * @package App\Http\Requests
 */
class ProcessOrderRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['exists:orders,id,status,' . Order::STATUS_NEW],
        ];
    }

    /**
     * Add parameters to be validated
     *
     * @return array
     */
    public function validationData()
    {
        return array_merge(
            parent::all(),
            $this->route()->parameters()
        );
    }
}
