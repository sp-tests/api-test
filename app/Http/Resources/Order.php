<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class Order
 * @package App\Http\Resources
 */
class Order extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        /**
         * @var \App\Models\Order $order
         */
        $order = $this->resource;

        return [
            'id'         => $order->id,
            'status'     => $order->status,
            'created_at' => $order->created_at,
        ];
    }
}
