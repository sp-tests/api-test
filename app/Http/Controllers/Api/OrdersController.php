<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Requests\ProcessOrderRequest;
use App\Http\Resources\Order as OrderResource;
use App\Http\Resources\OrderCollection;
use App\Jobs\ProcessOrder;
use App\Models\Order;
use App\Models\User;
use App\Services\OrdersService;
use Illuminate\Routing\Controller;
use Throwable;

/**
 * Class OrdersController
 * @package App\Http\Controllers\Api
 */
class OrdersController extends Controller
{
    /**
     * @var OrdersService
     */
    private OrdersService $ordersService;

    /**
     * OrdersController constructor.
     * @param OrdersService $ordersService
     */
    public function __construct(OrdersService $ordersService)
    {
        $this->ordersService = $ordersService;
    }

    /**
     * @param User $user
     * @return OrderCollection
     */
    public function userOrders(User $user): OrderCollection
    {
        $ordersQuery = $user->orders()
            ->orderBy('id')
            ->paginate();

        return new OrderCollection($ordersQuery);
    }

    /**
     * @param User $user
     * @return OrderResource
     * @throws Throwable
     */
    public function createForUser(User $user): OrderResource
    {
        $order = $this->ordersService->createOrderFor($user);

        return new OrderResource($order);
    }

    /**
     * @param ProcessOrderRequest $request
     * @return OrderResource
     * @throws Throwable
     */
    public function process(ProcessOrderRequest $request): OrderResource
    {
        $orderId = $request->route('id');
        /**
         * @var Order $order
         */
        $order = Order::query()->find($orderId);
        $order->update(['status' => Order::STATUS_PROCESSING]);

        try {
            dispatch(new ProcessOrder($order));
        } catch (Throwable $exception) {
            $order->update(['status' => Order::STATUS_NEW]);

            throw $exception;
        }

        return new OrderResource($order);
    }
}
