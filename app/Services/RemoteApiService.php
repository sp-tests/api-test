<?php

declare(strict_types=1);

namespace App\Services;

use App\Exceptions\Handler;
use App\Models\Order;
use Exception;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Client\ClientInterface;
use Throwable;

/**
 * Class RemoteApiService
 * @package App\Services
 */
class RemoteApiService
{
    /**
     * @var ClientInterface
     */
    private ClientInterface $apiClient;

    /**
     * @var Handler
     */
    private Handler $errorHandler;
    private string  $apiUri;

    /**
     * RemoteApiService constructor.
     * @param string $apiUri
     * @param ClientInterface $apiClient
     * @param Handler $errorHandler
     */
    public function __construct(string $apiUri, ClientInterface $apiClient, Handler $errorHandler)
    {
        $this->apiClient    = $apiClient;
        $this->errorHandler = $errorHandler;
        $this->apiUri       = rtrim($apiUri, '/');
    }

    /**
     * @param Order $order
     * @return bool
     * @throws Exception
     */
    public function orderApiAction(Order $order): bool
    {
        $status  = rand(0, 1) ? 200 : 400;
        $request = new Request('GET', "{$this->apiUri}/status/{$status}/");

        try {
            $this->apiClient->sendRequest($request);

            return true;
        } catch (Throwable $exception) {
            $this->errorHandler->report($exception);

            return false;
        }
    }
}
