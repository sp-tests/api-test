<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\Order;
use App\Models\User;
use Exception;
use Throwable;

/**
 * Class OrdersService
 * @package App\Services
 */
class OrdersService
{
    /**
     * @var RemoteApiService
     */
    private RemoteApiService $apiService;

    /**
     * OrdersService constructor.
     * @param RemoteApiService $apiService
     */
    public function __construct(RemoteApiService $apiService)
    {
        $this->apiService = $apiService;
    }

    /**
     * @param User $user
     * @return Order
     * @throws Throwable
     */
    public function createOrderFor(User $user): Order
    {
        $order = new Order(['status' => Order::STATUS_NEW]);
        $order->user()->associate($user);
        $order->saveOrFail();

        return $order;
    }

    /**
     * @param Order $order
     * @throws Exception
     */
    public function processOrder(Order $order): void
    {
        $processed = $this->apiService->orderApiAction($order);
        $newStatus = $processed ? Order::STATUS_PROCESSED : Order::STATUS_NEW;

        $order->update(['status' => $newStatus]);
    }
}
